<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login/login');
})->name('page-login');

Route::prefix('user')->group(function () {
    Route::get('/LoginPage', 'App\Http\Controllers\User\UserController@login')->name('user-login');
    Route::post('/LoginPagepost', 'App\Http\Controllers\User\UserController@login')->name('process-user-login');

    Route::get('/register', 'App\Http\Controllers\User\UserController@register')->name('user-register');
    Route::post('/registerpost', 'App\Http\Controllers\User\UserController@register')->name('process-user-register');
});
