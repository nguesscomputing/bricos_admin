<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="fr">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Login - BRICO SELLINGS</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Brico Sellings Dashboard">

    <meta name="msapplication-tap-highlight" content="no">
    <link rel="icon" type="image/x-jpeg" href="img/favicon.jpg">
    <link href="{{ URL::asset('style.css')}}" rel="stylesheet">
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div class="d-none d-lg-block col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-plum-plate"
                                        tabindex="-1">
                                        <div class="slide-img-bg"
                                            style="background-image: url('assets/images/originals/city.jpg');"></div>
                                        <div class="slider-content">
                                            <h3>Votre Système Dashboard</h3>
                                            <p>Connectez vous pour bénéficier de tous vos éléments de configuration avec votre système
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-premium-dark"
                                        tabindex="-1">
                                        <div class="slide-img-bg"
                                            style="background-image: url('assets/images/originals/citynights.jpg');">
                                        </div>
                                        <div class="slider-content">
                                            <h3>Évolutif, Modulaire, Cohérent</h3>
                                            <p>Excluez facilement les composants dont vous n'avez pas besoin, se base uniquement sur des modules adaptés au besoin de votre système
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-sunny-morning"
                                        tabindex="-1">
                                        <div class="slide-img-bg"
                                            style="background-image: url('assets/images/originals/citydark.jpg');">
                                        </div>
                                        <div class="slider-content">
                                            <h3>Complex</h3>
                                            <p>Vous découvrirez une large expansion de modules une discipline de manipulation est requise </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h-100 d-flex bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-9">
                            <div class="app-logo">
                                <img src="img/logo_brico.jpg" style="width:10rem"/>
                            </div>
                            <h4 class="mb-0">
                                <span class="d-block">Content de vous revoir,</span>
                                <span>Connectez-vous à votre compte s'il vous plaît.</span>
                            </h4>
                            <h6 class="mt-3">No account? <a href="{{route('user-register')}}" class="text-primary">S'inscrire
                                     à présent</a></h6>
                            <div class="divider row"></div>
                            <div>
                                <form class="">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleEmail" class="">Email</label>
                                                <input name="email" id="exampleEmail" placeholder="Email ici..."
                                                    type="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePassword" class="">Mot de passe</label>
                                                <input name="password" id="examplePassword"
                                                    placeholder="Mot de passe here..." type="password" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="position-relative form-check">
                                        <input name="check" id="exampleCheck" type="checkbox"
                                            class="form-check-input">
                                        <label for="exampleCheck" class="form-check-label">Rester connecté</label>
                                    </div>
                                    <div class="divider row"></div>
                                    <div class="d-flex align-items-center">
                                        <div class="ml-auto">
                                            <a href="javascript:void(0);" class="btn-lg btn btn-link">Récupérer
                                                 Mot de passe</a>
                                            <button class="btn btn-primary btn-lg">Connectez-vous au tableau de bord</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ URL::asset('assets/scripts/script.js')}}"></script>
</body>

</html>
