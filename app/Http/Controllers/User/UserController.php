<?php

namespace App\Http\Controllers\User;

use App\Models\Role;
use Illuminate\Routing\Controller as BaseController;  // <<< See here - no real class, only an alias

use App\Models\User;
use Illuminate\Support\Facades\Auth; // les facades sont comme les interfaces
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

class UserController extends BaseController
{
    //
    /* fonction pour le login*/ // gerer le logout lors de l'appel de la vue si il est deja connecte
    function login(Request $request){
        if($request->isMethod('POST'))
        {
            $remember = ($request->remember) ? true : false;
            // $model = User::class;
            if (!User::where('email',$request->email)->exists()) {
                return response()->json([
                    'status'=>'info',
                    'message'=>'Utilisateur pas trouve!!'
                ]);
            }
            if (Auth::attempt(['email' => $request->email,'password' => $request->password], $remember)) {

                $user=Auth::user();
                $user->statut = 1;
                $user->save();
                /*if ($user->role=="user") {
                    return response()->json([
                        'status'=>'success',
                        'code'=>1
                    ]);
                }
                if ($user->role=="admin") {
                    return response()->json([
                        'status'=>'success',
                        'code'=>2
                    ]);
                }*/

                return response()->json([
                    'status'=>'success',
                    'code'=>1
                ]);

            }
            else{
                return response()->json([
                    'status'=>'info',
                    'message'=>'Identifiants de login invalides,Veuillez reessayer plutard!!!'
                ]);
            }
            if (Auth::viaRemember())
            {

            }
        }
        else if($request->isMethod('GET'))
        {
            return view('user.login');
        }
        else
        {
            return response()->json([
                'status'=>'info',
                'message'=>'Methode de requete invalide'
            ]);
        }
    }

    //fonction pour register view
    function register(Request $request){
        if ($request -> isMethod('POST')){
            //dd($request->password);
            if (User::where('email',$request->email)->exists())
            {
                return response()->json([
                    'status'=>'info',
                    'message'=>'Cet Email existe deja '
                ]);
            }
            else if (User::where('pseudo',$request->username)->exists())//$request->pseudo ici car c'est pseudo dans la bd et on verifie
            {
                return response()->json([
                    'status'=>'info',
                    'message'=>'Ce pseudo existe deja'
                ]);
            }
            else
            {
            $users = User::create([
                'email'=>$request->email,
                'localisation'=>$request->localisation,
                'prenom'=>$request->prenom,
                'name'=>$request->nom,
                'role'=>$request->role,
                'tel'=>$request->tel,
                'pseudo'=>$request->username,
                'password'=>Hash::make($request->password),
                'remember_token' => Str::random(10),
            ]);
                if ($users) {
                    /*try {
                        Mail::to($request->email)->queue(
                            new NewUserNotification()
                        );

                    } catch (\Exception $e) {
                        // return $e->getMessage();
                        return response()->json([
                            'status'=>'info',
                            'message'=>'Action failed ,Try again later!'
                        ]);
                    }*/
                    return response()->json([
                        'status'=>'success',
                        'message'=>'l\'enregistrement s\'effectue avec succes. Veuillez vous connecter'
                    ]);
                } else {
                    return response()->json([
                        'status'=>'info',
                        'message'=>'Erreur lors de l\'enregistrement. Veuillez reessayer plutard'
                    ]);
                }
            }

        }else if ($request ->isMethod('GET')){
            return view('register.register');
        }else{
            return response() ->json([
                'status'=>'info',
                'message' =>'Methode de requete invalide'
            ]);
        }
    }

    //fonction pour logout
    public function userlogout(Request $request)
    {
        $user=Auth::user();
        //$user->online = false;
        $user->statut = 0; //pour le statut online ou off
        $user->save();
        Auth::logout();
        return redirect()->route('page-login');
    }
}
