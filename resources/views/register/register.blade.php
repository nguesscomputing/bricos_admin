<!doctype html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="fr">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Inscription - BRICO SELLINGS</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Brico Sellings Dashboard">

    <meta name="msapplication-tap-highlight" content="no">
    <link rel="icon" type="image/x-jpeg" href="{{ URL::asset('img/favicon.jpg')}}">
    <link href="{{ URL::asset('style.css')}}" rel="stylesheet">
</head>

<body>
    <div class="app-container app-theme-white body-tabs-shadow">
        <div class="app-container">
            <div class="h-100">
                <div class="h-100 no-gutters row">
                    <div
                        class="h-100 d-md-flex d-sm-block bg-white justify-content-center align-items-center col-md-12 col-lg-8">
                        <div class="mx-auto app-login-box col-sm-12 col-md-10 col-lg-11">
                            <div class="app-logo">
                                <img src="{{ URL::asset('img/logo_brico.jpg')}}" style="width:8rem"/>
                            </div>
                            <h4>
                                <div>Bienvenue chez BRICO SELLING,</div>
                                <span>Cela ne prend que <span class="text-success">quelques secondes</span> pour créer votre
                                     compte</span>
                            </h4>
                            <div>
                                <form onsubmit="return FormRegister()" id="register-user">
                                    <div class="form-row">
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleEmail" class=""><span
                                                        class="text-danger">*</span> Email</label>
                                                <input name="email" id="exampleEmail" placeholder="Email ici..."
                                                    type="email" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleTel" class=""><span
                                                        class="text-danger">*</span>Numéro de téléphone</label>
                                                <input name="tel" id="exampleTel" placeholder="Numéro de téléphone ici..."
                                                    type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleNom" class=""><span
                                                        class="text-danger">*</span> Nom</label>
                                                <input name="nom" id="exampleNom" placeholder="Nom ici..."
                                                    type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePrenom" class=""><span
                                                        class="text-danger">*</span>Prénom</label>
                                                <input name="prenom" id="examplePrenom" placeholder="Prénom ici..."
                                                    type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePseudo" class=""><span
                                                        class="text-danger">*</span> Pseudo / Username</label>
                                                <input name="pseudo" id="examplePseudo" placeholder="Pseudo ici..."
                                                    type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="exampleLocalisation" class=""><span
                                                        class="text-danger">*</span>Localisation</label>
                                                <input name="localisation" id="exampleLocalisation" placeholder="Localisation ici..."
                                                    type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePassword" class=""><span
                                                        class="text-danger">*</span> Mot de passe</label>
                                                <input name="password" id="examplePassword"
                                                    placeholder="Mot de passe here..." type="password" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="position-relative form-group">
                                                <label for="examplePasswordRep" class=""><span
                                                        class="text-danger">*</span> Répéter le mot de passe</label>
                                                <input name="passwordrep" id="examplePasswordRep"
                                                    placeholder="Répéter le mot de passe ici..." type="password"
                                                    class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="position-relative form-group">
                                                <label for="exampleRole" class=""><span
                                                        class="text-danger">*</span> Roles / Privillèges</label>
                                                <select name="role" id="exampleRole" class="form-control">
                                                    <option value="Subscriber">Subscriber</option>
                                                    <option value="Contributor">Contributor</option>
                                                    <option value="Author">Author</option>
                                                    <option value="Editor">Editor</option>
                                                    <option value="Administrator">Administrator</option>
                                                    <option value="Nothing" selected="selected">-No role for this system-</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-3 position-relative form-check">
                                        <input name="check" id="exampleCheck" type="checkbox"
                                            class="form-check-input">
                                        <label for="exampleCheck" class="form-check-label">Acceptez nos <a
                                                 href="javascript:void(0);">Termes et conditions</a>.</label>
                                    </div>
                                    <div class="mt-4 d-flex align-items-center">
                                        <h6 class="mb-0">Avez vous déjà un compte ? <a href="{{route('page-login')}}"
                                                class="text-primary">Se connecter</a></h6>
                                        <div class="ml-auto">
                                            <button
                                                class="btn-wide btn-pill btn-shadow btn-hover-shine btn btn-primary btn-lg" style="width:14rem" type="submit">Créer un compte </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="d-lg-flex d-xs-none col-lg-4">
                        <div class="slider-light">
                            <div class="slick-slider slick-initialized">
                                <div>
                                    <div class="position-relative h-100 d-flex justify-content-center align-items-center bg-premium-dark"
                                        tabindex="-1">
                                        <div class="slide-img-bg"
                                            style="background-image: url('{{ URL::asset('assets/images/originals/citynights.jpg')}}');">
                                        </div>
                                        <div class="slider-content">
                                            <h3>Évolutif, Modulaire, Cohérent</h3>
                                            <p>Excluez facilement les composants dont vous n'avez pas besoin, se base uniquement sur des modules adaptés au besoin de votre système
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="{{ URL::asset('assets/scripts/script.js')}}"></script>
    <!--================begin link js=================-->
    @include('partials.bottom')
    <!--================end link js=================--> 

    <script>
        function FormRegister() {
            PNotify.defaults.destroy = false;
            PNotify.defaultModules.set(PNotifyMobile, {});
            PNotify.defaultModules.set(PNotifyFontAwesome5Fix, {});

            var info = {};
            var errors = [];
            // var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

            $("#register-user")
                .find("input")
                .each(function() {
                    $(this).css("border", "1px solid #ccc");
                    if ($(this).val() == "" || $(this).val() == null) {
                        $(this).css("border", "1px solid #f77575");
                        errors.push($(this).attr("name"));
                    }
                    info[$(this).attr("name")] = $(this).val();
                });

                if(!$("#exampleCheck").is(':checked')) {
                    errors.push($("#exampleCheck").attr("name"));
                }

                if($("#examplePassword").val()!=$("#examplePasswordRep").val()) {
                    $("#examplePassword").css("border", "1px solid #f77575");
                    $("#examplePasswordRep").css("border", "1px solid #f77575");
                    errors.push($("#examplePassword").attr("name"));
                }

                if (errors.length > 0) {
                    PNotify.error({
                        title: 'Champs vides',
                        text: "Erreur remplissez tous vos champs marqués de l'astérix rouge",
                        delay: 4000
                    });
                    return false;
                }
                info[$("#exampleRole").attr("name")] = $("#exampleRole").val();

                processRegister(info);

                return false;
        }

        function processRegister(info) {
            const notice = PNotify.notice({
                title: 'Confirmation demandée',
                text: 'Etes vous sûr?',
                icon: 'fa fa-question-circle',
                hide: false,
                destroy: true,
                closer: false,
                sticker: false,
                modules: new Map([
                    ...PNotify.defaultModules,
                    [PNotifyConfirm, {
                    confirm: true
                    }]
                ])
                });
                notice.on('pnotify:confirm',  () => httpAjaxLaravel(info));
        }

        function httpAjaxLaravel(info) {
            $.ajax({

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },

                type:'POST',

                data:info,

                url: '{{route('process-user-register')}}',

                success:function(response){
                    if(response.status=='success'){
                        PNotify.success({
                            title: 'Success',
                            text: response.message,
                            delay: 3000
                        });
                        location.reload(true);
                    }

                    if(response.status=='info'){
                        PNotify.warning({
                            title: 'Info connexion',
                            text: response.message,
                            delay: 3000
                        });
                        location.reload(true);
                    }
                },

                error:function(response){
                    PNotify.error({
                        title: 'Erreur de connexion',
                        text: 'Erreur de connexion, Veuillez reesayer plutard',
                        delay: 4000
                    });
                    location.reload(true);
                }
            })

        }

    </script> 
</body>

</html>