<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
// import builder where defaultStringLength method is defined
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191); //Update defaultStringLength
    }
}