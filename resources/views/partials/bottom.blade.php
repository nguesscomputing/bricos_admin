<!-- jquery-->
<script src="{{ URL::asset('js/jquery-2.2.4.min.js')}}" type="text/javascript"></script>



<link rel="stylesheet" href="{{ URL::asset('packages/PNotify.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/BrightTheme.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/Material.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/Angeler.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyBootstrap4.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyMobile.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyDesktop.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyConfirm.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyCountdown.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyPaginate.css')}}" />
<link rel="stylesheet" href="{{ URL::asset('packages/PNotifyReference.css')}}" />

<script src="{{ URL::asset('packages/PNotify.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyBootstrap4.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyFontAwesome5Fix.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyFontAwesome5.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyAnimate.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyMobile.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyDesktop.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyConfirm.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyCountdown.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyPaginate.js')}}"></script>
<script src="{{ URL::asset('packages/PNotifyReference.js')}}"></script>